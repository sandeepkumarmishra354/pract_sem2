import { Person } from "./person";

// extend Employee as Person. Because every employee is a person also.
export class Employee extends Person {
    private employeeID = "";

    constructor(name: string, email: string, empId: string) {
        super(name, email);
        this.employeeID = empId;
    }

    public getEmployeeID = () => {
        return this.employeeID;
    }
}