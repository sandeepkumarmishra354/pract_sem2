import { Component, OnInit } from '@angular/core';
import { Item } from '../item';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  // Adding property
  //description="Description is property of item";
  description: Item = { item_Name: "Item name 1", item_Description: "This is full description of item name 1" };

  constructor() { }

  ngOnInit(): void {
  }

}
