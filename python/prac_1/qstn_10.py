# Write a Python program to count the number of characters (character frequency) in a string.
# Sandeep Mishra (2012093)

source = input("Enter source string: ")
char_to_search = input("Enter character to count frequency in source: ")
frequency = 0

if len(char_to_search) != 1:
    print("character should be of 1 length")
else:
    for ch in source:
        if ch == char_to_search:
            frequency += 1
    print("frequency is: ", frequency)
