class Employee:
    def __init__(self):
        # create an empty list of employees
        self.employees = []
    
    def add(self, name, department):
        emp = {"name": name, "dep": department}
        self.employees.append(emp)
        return emp

    def list_all(self):
        print()
        print("Total employees: "+str(len(self.employees)))
        print("-------------------------------------------")
        print("------------- Employee list ---------------")
        print("-------------------------------------------")
        for emp in self.employees:
            details = "Name: {}, Department: {}".format(emp["name"], emp["dep"])
            print(details)
        print("-------------------------------------------")
        print()

employee = Employee()

employee.add("sandeep", "dep 1")
employee.add("name 1", "dep 1")
employee.add("name 2", "dep 1")
employee.add("name 5", "production")

employee.list_all()
