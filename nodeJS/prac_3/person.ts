export class Person {
    private fullName = "";
    private email = "";

    constructor(name: string, email: string) {
        this.fullName = name;
        this.email = email;
    }

    public getFullName = () => {
        return this.fullName;
    }

    public getEmail = () => {
        return this.email;
    }
}