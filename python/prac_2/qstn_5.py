def merge_dict(dict1, dict2):
    return {**dict1, **dict2}

merged_dict = merge_dict({'firstname':'sandeep'}, {'lastname':'mishra'})

print(merged_dict)