<?php
    // expects marks of type integer
    function check_performance($marks) {
        if($marks == 0) {
            return "You were absent. Meet class co-ordinator.\n";
        } else if($marks >= 1 && $marks <= 10) {
            return "Your performance is poor. Solve whole paper again and submit.\n";
        } else if($marks >= 11 && $marks <= 15) {
            return "You have scored less. Appear for a retest.\n";
        } else if($marks >= 16 && $marks <= 20) {
            return "You have done well. Can do better.\n";
        } else if($marks > 20 && $marks <= 25) {
            return "Outstanding performance. Keep it up!\n";
        } else {
            return "Only upto 25 marks allowed.\n";
        }
    }
?>