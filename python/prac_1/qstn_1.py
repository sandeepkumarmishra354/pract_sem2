## Write a python program to swap two numbers using a third variable
## Sandeep Mishra (2012093)

num1 = int(input("Enter num 1: "))
num2 = int(input("Enter num 2: "))

print(num1)
print(num2)

print("after swap using third variable")

tmp = num1
num1 = num2
num2 = tmp

print(num1)
print(num2)