# Write a recursive function to calculate the sum of numbers from 0 to 10

# constraint: start < end (always)
def calculate_sum(start, end):
    if start > end:
        print("start value must be less than or equal to end value.")
        return
    if start == end:
        return end
    return start + calculate_sum(start+1, end)


sum = calculate_sum(0, 10)

print(sum)
