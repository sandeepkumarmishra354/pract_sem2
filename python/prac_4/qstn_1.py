class Calculator:
    def add(self, num1, num2):
        return num1 + num2

    def subtract(self, num1, num2):
        return num1 - num2

    def multiply(self, num1, num2):
        return num1 * num2

    def divide(self, num1, num2):
        return num1 / num2


my_calculator = Calculator()

print(my_calculator.add(5,7))
print(my_calculator.subtract(5,7))
print(my_calculator.multiply(5,7))
print(my_calculator.divide(15,5))
