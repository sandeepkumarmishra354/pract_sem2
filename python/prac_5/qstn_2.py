## Write a Python program to copy the contents of a file to another file.

import os

source_file = input("Enter source file: ")
dest_file = input("Enter destination file: ")

print("copying...")

## simply run a shell command to copy the file.
os.popen("cp {} {}".format(source_file, dest_file))

print("file copied.")