# Write a python program to read radius of a circle and print the area
# Sandeep Mishra (2012093)

PI = 3.14
radius = float(input("Enter radius of circle: "))

area = PI*(radius**2)

print("Area is: ", area, " square unit")
