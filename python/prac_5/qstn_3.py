## Write a Python program to count the number of lines in a text file.

with open("dummy.txt", "r") as f:
    num_of_lines = len(f.readlines())
    print("Number of line(s) in file is: {}".format(num_of_lines))
