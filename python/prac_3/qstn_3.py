# Write a Python script to display the various Date Time formats.
import datetime as DT

today = DT.datetime.today()

print("date & time: "+str(today))
print("year: "+str(today.year))
print("month: "+str(today.month))
print("week : "+str(today.weekday()))
print("day : "+str(today.day))