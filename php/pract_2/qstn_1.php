<!DOCTYPE html>

<head>
	<title>Calculator</title>
    <style>
    	#container {
        	width: 100vw;
            height: 100vh;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
        .btn {
            padding: 12px;
        }
        .inp {
            padding: 12px;
        }
    </style>
</head>

<?php
    include("my_file.php");
?>

<body>
    <div id="page-wrap">
	  <div id="container">
          <h1>Calculator</h1>
          <form action="" method="post" id="quiz-form">
            <p>
                <input
                class="inp" type="number" name="first_num" id="first_num"
                required="required" value="<?php echo $first_num; ?>" />
                <b>First Number</b>
            </p>
            <p>
                <input
                class="inp" type="number" name="second_num" id="second_num"
                required="required" value="<?php echo $second_num; ?>" />
                <b>Second Number</b>
            </p>
            <p>
                <input
                class="inp" readonly="readonly" name="result"
                value="<?php echo $result; ?>"/>
                <b>Result</b>
            </p>
            <input class="btn" type="submit" name="operator" value="Add" />
            <input class="btn" type="submit" name="operator" value="Subtract" />
            <input class="btn" type="submit" name="operator" value="Multiply" />
            <input class="btn" type="submit" name="operator" value="Divide" />
	  </form>
      </div>
    </div>
</body>
</html>