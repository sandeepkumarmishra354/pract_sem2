# Write a Python program to read first n lines of a file.

num_of_line = int(input("Enter num of line to read: "))
print()  # print a new line character
lines = []

with open("dummy.txt", "r") as f:
    for _ in range(num_of_line):
        line = f.readline()
        if len(line) > 0:
            lines.append(line)

for line in lines:
    print(line, end = "")

print()  # print a new line character
