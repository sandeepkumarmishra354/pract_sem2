# Write a python program to read a number, if it is an even number,
# print the square of that number and if it is odd number print cube of that number.
# Sandeep Mishra (2012093)

num = int(input("Enter a number: "))

if num % 2 == 0:
    print(pow(num, 2))
else:
    print(pow(num, 3))
