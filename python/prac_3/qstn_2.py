# Given a list slice it into 3 equal chunks and reverse each chunk.

# slices the list into given parts as steps

def slice_list(arr, step):
    new_lists = []
    if len(arr) % step == 0:
        end = step
        for i in range(0, len(arr), step):
            sliced_chunk = arr[i:end]
            new_lists.append(sliced_chunk[::-1]) # reverse the list
            end += step
    return new_lists


my_list = [11, 45, 8, 23, 14, 12, 78, 45, 89]

print("original list: "+str(my_list))

print("----------------------------------------------------------")

print("sliced & reversed list: "+str(slice_list(my_list, 3)))
